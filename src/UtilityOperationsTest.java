import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by nick on 3/31/15.
 */
public class UtilityOperationsTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testOverflow() throws Exception {

    }

    @Test
    public void testUnderflow() throws Exception {

    }

    @Test
    public void testUpperHalf() throws Exception {

    }

    @Test
    public void testLowerHalf() throws Exception {

    }

    @Test
    public void testDigits() throws Exception {

    }

    @Test
    public void testPositiveAbsoluteValue() throws Exception {
        //basic positive absolute value test
        int a = 100;
        assertEquals(a, UtilityOperations.absoluteValue(a));
    }

    @Test
    public void testNegativeAbsoluteValue() throws Exception {
        //basic positive absolute value test
        int a = -100;
        int expected = 100;
        assertEquals(expected, UtilityOperations.absoluteValue(a));
    }

}