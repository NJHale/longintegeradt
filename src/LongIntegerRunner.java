import java.util.Dictionary;
import java.util.Hashtable;
import java.util.ArrayList;

public class LongIntegerRunner {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();
        //instantiate test cases
        Dictionary longIntDict = new Hashtable();
        longIntDict.put('A', new LongInteger("2222"));
        longIntDict.put('B', new LongInteger("99999999"));
        longIntDict.put('C', new LongInteger("-246813575732"));
        longIntDict.put('D', new LongInteger("180270361023456789"));
        longIntDict.put('E', new LongInteger("1423550000000010056810000054593452907711568359"));
        longIntDict.put('F', new LongInteger("-350003274594847454317890"));
        longIntDict.put('G', new LongInteger("29302390234702973402973420937420973420937420937234872349872934872893472893749287423847"));
        longIntDict.put('H', new LongInteger("-98534342983742987342987339234098230498203894209928374662342342342356723423423"));
        runStep1TestCases(longIntDict);
        runStep2TestCases(longIntDict);
        long elapsedTime = System.currentTimeMillis() - start;
        System.out.println("\nMilliseconds Elapsed: " + elapsedTime);
    }

    public static void runStep1TestCases(Dictionary longIntDict) {
        //iterate through test cases and create/store LongIntegers

        //START - LongInteger testing
        for (char key = 'A'; key <= 'H'; key++) {
            LongInteger longInt = (LongInteger)longIntDict.get(key);
            System.out.println(key);
            System.out.println("Value of each node: "); longInt.outputNodes();
            System.out.print("LongInteger to standard output: "); longInt.output();
            String sign = "+";
            if (longInt.getSign())
                sign = "-";
            System.out.println("\nLongInteger sign: " + sign);
            System.out.println("LongInteger #digits: " + longInt.getDigitCount());
        }
        //END - LongInteger testing

        //START - UtilityOperations testing
        int a = 2222;
        int b = 99999999;
        System.out.println();
        applyUtilityMethods(a);
        applyUtilityMethods(b);
        System.out.println();
        //END - UtilityOperations testing

        //START - Comparison testing
        for(char keyA = 'A'; keyA <= 'H'; keyA++) {
            LongInteger longIntA = (LongInteger)longIntDict.get(keyA);
            for(char keyB = keyA; keyB <= 'H'; keyB++) {
                LongInteger longIntB = (LongInteger)longIntDict.get(keyB);
                //check greater than
                System.out.println(keyA + " > " + keyB + " --> " +
                        longIntA.greaterThan(longIntB));
                //check less than
                System.out.println(keyA + " < " + keyB + " --> " +
                        longIntA.lessThan(longIntB));
                //check equal to
                System.out.println(keyA + " == " + keyB + " --> " +
                        longIntA.equalTo(longIntB));
            }
        }
        //END - Comparison testing
    }

    /**
     * apply testcases for step-2 of the project
     * @param longIntDict
     */
    public static void runStep2TestCases(Dictionary longIntDict) {
        //test add
        for(char keyA = 'A'; keyA <= 'H'; keyA++) {
            LongInteger longIntA = (LongInteger)longIntDict.get(keyA);
            for(char keyB = keyA; keyB <= 'H'; keyB++) {
                LongInteger longIntB = (LongInteger)longIntDict.get(keyB);
                //long integer add
                System.out.print(keyA + " + " + keyB + " --> "); longIntA.add(longIntB).output();
            }
        }
        //test subtract
        for(char keyA = 'A'; keyA <= 'H'; keyA++) {
            LongInteger longIntA = (LongInteger)longIntDict.get(keyA);
            for(char keyB = keyA; keyB <= 'H'; keyB++) {
                LongInteger longIntB = (LongInteger)longIntDict.get(keyB);
                //long integer add
                System.out.print(keyA + " - " + keyB + " --> "); longIntA.subtract(longIntB).output();
            }
        }
        //test multiply
        for(char keyA = 'A'; keyA <= 'H'; keyA++) {
            LongInteger longIntA = (LongInteger)longIntDict.get(keyA);
            for(char keyB = keyA; keyB <= 'H'; keyB++) {
                LongInteger longIntB = (LongInteger)longIntDict.get(keyB);
                //long integer add
                System.out.print(keyA + " * " + keyB + " --> "); longIntA.multiply(longIntB).output();
            }
        }

        //instantiate test cases
        longIntDict.put('I', ((LongInteger)longIntDict.get('A')).add((LongInteger)longIntDict.get('D')));
        longIntDict.put('J', ((LongInteger)longIntDict.get('B')).add((LongInteger)longIntDict.get('C')));
        longIntDict.put('K', ((LongInteger)longIntDict.get('C')).add((LongInteger)longIntDict.get('D')));
        longIntDict.put('L', ((LongInteger)longIntDict.get('I')).add((LongInteger)longIntDict.get('I')));
        longIntDict.put('M', ((LongInteger)longIntDict.get('A')).add((LongInteger)longIntDict.get('I')));
        longIntDict.put('N', ((LongInteger)longIntDict.get('B')).add((LongInteger)longIntDict.get('K')));
        longIntDict.put('O', ((LongInteger)longIntDict.get('A')).subtract((LongInteger)longIntDict.get('D')));
        longIntDict.put('P', ((LongInteger)longIntDict.get('C')).subtract((LongInteger)longIntDict.get('D')));
        longIntDict.put('Q', ((LongInteger)longIntDict.get('D')).subtract((LongInteger)longIntDict.get('C')));
        longIntDict.put('R', ((LongInteger)longIntDict.get('L')).subtract((LongInteger)longIntDict.get('L')));
        longIntDict.put('S', ((LongInteger)longIntDict.get('P')).subtract((LongInteger)longIntDict.get('O')));
        longIntDict.put('T', ((LongInteger)longIntDict.get('N')).subtract((LongInteger)longIntDict.get('Q')));
        longIntDict.put('U', ((LongInteger)longIntDict.get('A')).multiply((LongInteger)longIntDict.get('D')));
        longIntDict.put('V', ((LongInteger)longIntDict.get('B')).multiply((LongInteger)longIntDict.get('C')));
        longIntDict.put('W', ((LongInteger)longIntDict.get('D')).multiply((LongInteger)longIntDict.get('D')));
        longIntDict.put('X', ((LongInteger)longIntDict.get('O')).multiply((LongInteger)longIntDict.get('I')));
        longIntDict.put('Y', ((LongInteger)longIntDict.get('J')).multiply((LongInteger)longIntDict.get('P')));
        longIntDict.put('Z', ((LongInteger)longIntDict.get('M')).multiply((LongInteger)longIntDict.get('N')));

        //output testcases
        for(char key = 'I'; key <= 'Z'; key++) {
            System.out.print(key + " --> "); ((LongInteger)longIntDict.get(key)).output();

        }
    }

    public static void applyUtilityMethods(int v) {
        System.out.println("Applying util methods on: " + v);
        System.out.println("Overflow: " + UtilityOperations.overflow(v));
        System.out.println("Underflow: " + UtilityOperations.underflow(v));
        System.out.println("Upper half: " + UtilityOperations.upperHalf(v));
        System.out.println("Lower half: " + UtilityOperations.lowerHalf(v));
        System.out.println("Digits: " + UtilityOperations.digits(v));
    }
}
