
public class LongInteger {
    
	// DO NOT CHANGE OR REMOVE THIS LINE (UNTIL STEP 3)
    private ProjectList list = new DLLProjectList();

    //flag to see if the LongInteger is negative
    private boolean isNeg;

    public LongInteger(String s) {
        //get the sign of the LongInteger
        isNeg = (s.charAt(0) == '-');
        //if it's negative trim off the sign
        if (isNeg) { s = s.substring(1); }
        //start filling the ProjectList
        int i;//declare outside loop to preserve scope
        for (i = s.length(); i - 8 >= 0; i-=8) {//make offsets
            //get the next 8 digits of the LongInteger
            int value = Integer.parseInt(
                    s.substring(i - 8, i));
            //insert the next 8 digits into the ProjectList
            list.insertFirst(value);
        }
        //get the remaining most significant digits
        if (i > 0) {
            int mostSig = Integer.parseInt(
                    s.substring(0, i));
            list.insertFirst(mostSig);
        }
    }

    /**
     * private constructor for instantiating a LongInteger
     * using a ProjectList
     */
    private LongInteger (ProjectList list, boolean isNeg) {
        this.list = list;
        this.isNeg = isNeg;
    }

    public void outputNodes() {
        if (!list.isEmpty()) {
            Position node = list.first();
            boolean transversed = false;
            while (!transversed) {
                int value = node.getValue();
                //check to see if we've fully transversed the list
                if (list.isLast(node))
                    transversed = true;
                else
                    node = list.after(node);
            }
        }
    }


    public void output() {
        String longIntStr = "";
        if (!list.isEmpty()) {
            Position node = list.first();
            boolean transversed = false;
            while (!transversed) {
                int value = node.getValue();
                int digits = UtilityOperations.digits(value);
                //if not the most significant node pad with zeros
                if (!list.isFirst(node)) {
                    for (int i = 0; i < 8 - digits; i++) {//add leading zeros
                        longIntStr += "0";
                    }
                }
                longIntStr += value;
                //check to see if we've fully transversed the list
                if (list.isLast(node))
                    transversed = true;
                else
                    node = list.after(node);
            }
        }
        //append respective sign
        longIntStr = isNeg ? ("-" + longIntStr) : ("" + longIntStr);
        //send to output stream
        System.out.println(longIntStr);
    }


	public boolean getSign() {
        return isNeg;
	}

	public int getDigitCount() {
        int mostSig = list.first().getValue();
        return ((list.size() - 1)*8 +
                UtilityOperations.digits(mostSig));
	}


    public boolean equalTo(LongInteger i) {
        //check for sign difference
        if (this.getSign() != i.getSign())
            return false;
        //check if there is a magnitude difference
        if (this.getDigitCount() != i.getDigitCount())
            return false;
        //perform digit by digit check
        Position nodeA = list.last();
        Position nodeB = i.list.last();
        boolean transversed = false;
        while (!transversed) {
            if (nodeA.getValue() != nodeB.getValue())
                return false;
            if (list.isLast(nodeA)) {
                transversed = true;
            }
            else {
                nodeA = list.before(nodeA);
                nodeB = list.before(nodeB);
            }
        }
        return true;
    }


    public boolean lessThan(LongInteger i) {
        //check if we are comparing negative to positive
        if (this.getSign() && !i.getSign())
            return true;
        //check if we are comparing positive to negative
        if (!this.getSign() && i.getSign())
            return false;
        //check if the LongIntegers are equal
        if (this.equalTo(i))
            return false;
        //check if we are comparing negative to negative
        if (this.getSign() && i.getSign()) {
            //check magnitudes
            if (this.list.size() > i.list.size())
                return true;
            if (this.list.size() < i.list.size())
                return false;
            //implicit same size, now check most significant digit
            //since they are both negative the largest is actually less than
            if (this.list.last().getValue() >
                    i.list.last().getValue()) {
                return true;
            }
            return false;
        }
        //implicit both positive
        //check magnitudes
        if (this.list.size() < i.list.size())
            return true;
        if (this.list.size() > i.list.size())
            return false;
        //implicit same size, now check most significant digit
        if (this.list.last().getValue() <
                i.list.last().getValue()) {
            return true;
        }
        return false;
    }

    public boolean greaterThan(LongInteger i) {
        return !(this.lessThan(i) || this.equalTo(i));
    }

    public LongInteger add(LongInteger i) {
        //handle mismatched sign occurrences
        if (!this.isNeg && i.isNeg)
            return this.subtract(i);
        else if (this.isNeg && !i.isNeg)
            return i.subtract(this);

        //find and assign values based on relational magnitude
        ProjectList larger, smaller;
        if (i.list.size() < this.list.size()) {
            larger = this.list;
            smaller = i.list;
        }else {//greater than or equal to
            larger = i.list;
            smaller = this.list;
        }
        //implicit same sign; can now perform general addition algorithm
        //instantiate new ProjectList to represent the sum
        ProjectList sumList = new DLLProjectList();
        //create and set transversal nodes for larger and smaller
        Position lrgNode = larger.last();
        Position smlNode = smaller.last();
        //initialize the overflow
        int overflow = 0;
        //transverse both ProjectLists adding the sum of each node set the sumList
        //instantiate flag for transversal of smaller
        boolean transversedSml = false;
        while (!transversedSml) {
            //check to see if this is the first node in smaller
            transversedSml = smaller.isFirst(smlNode);
            //calculate summation of nodes and previous overflow
            int value = smlNode.getValue() +
                    lrgNode.getValue() + overflow;
            //calculate underflow for radix 8
            int underflow = UtilityOperations.
                    underflow(value);
            //insert underflow into sumList
            sumList.insertFirst(underflow);
            //move nodes
            lrgNode = larger.before(lrgNode);
            smlNode = smaller.before(smlNode);
            //calculate next overflow for radix 8
            overflow = UtilityOperations.
                    overflow(value);
        }

        //handle end case where larger is of a higher magnitude than smaller
        boolean transversedLrg = false;
        while (!transversedLrg && lrgNode != null) {
            //check to see if this is the last node in larger
            transversedLrg = larger.isFirst(lrgNode);
            //calculate the summation of lrgNode and overflow
            int value = lrgNode.getValue() + overflow;
            //calculate underflow for radix 8
            int underflow = UtilityOperations.
                    underflow(value);
            //insert underflow into sumList
            sumList.insertFirst(underflow);
            //move node
            lrgNode = larger.before(lrgNode);
            //calculate overflow for radix 8
            overflow = UtilityOperations.
                    overflow(value);
        }

        //handle insertion of final overflow if any
        if (overflow > 0) {
            sumList.insertFirst(overflow);
        }

        //instantiate new LongInteger using sumList
        //sum will have the same sign as both it's parts
        LongInteger sum = new LongInteger(sumList, this.isNeg);
        //return LongInteger representation of the some of LongInteger this and i
        return sum;

    }

    public LongInteger subtract(LongInteger i) {
        //handle addition cases
        if (!this.isNeg && i.isNeg) {// a - (-b) => a + b
            //create a clone to maintain object integrity
            LongInteger iClone = i.clone();
            //switch sign for addition
            iClone.isNeg = false;
            return this.add(iClone);
        }
        if (this.isNeg && !i.isNeg) {// (-a) - (b) => -(a + b)
            //create a clone to maintain object integrity
            LongInteger thisClone = this.clone();
            //switch sign for addition
            thisClone.isNeg = false;
            LongInteger result = thisClone.add(i);
            result.isNeg = true;
            return result;
        }
        if (this.isNeg && i.isNeg) {// (-a) - (-b) => (-a) + b => b - a
            LongInteger iClone = i.clone();LongInteger thisClone = this.clone();
            iClone.isNeg = false;thisClone.isNeg = false;
            return iClone.subtract(thisClone);
        }

        ProjectList listA, listB;
        //handle setting the sign of the result
        boolean isNeg;
        if (this.equalTo(i)) {
            return new LongInteger("0");//if they are equal we know the result is 0
        } else if (this.lessThan(i)) {
            isNeg = true;
            listA = i.list;
            listB = this.list;
        } else {
            isNeg = false;
            listA = this.list;
            listB = i.list;
        }

        //implicitly reduced to case: this, i > 0
        Position nodeA = listA.last();
        Position nodeB = listB.last();
        ProjectList subList = new DLLProjectList();
        //create and initialize subCarry to implement carry
        int subCarry = 0;
        //transverse until most sig position
        boolean transversedA = false;
        boolean transversedB = false;
        while (!(transversedA || transversedB)) {
            //98534342983742987342987339234098230498203894209928374312339067747509169105532
            //98534342983742987342987339234098230498203894209928374312339067747509269105533
            //flip transversed flag if the end has been reached in a node
            transversedA = listA.isFirst(nodeA);
            transversedB = listB.isFirst(nodeB);
            int sub = nodeA.getValue() -
                    nodeB.getValue() - subCarry;
            //handle if the sub goes negative
            if (sub < 0) {
                subCarry = 1;
                sub = 99999999 + sub + 1;
            }
            else {
                subCarry = 0;
            }
            //insert sub into sumList
            subList.insertFirst(sub);
            //move nodes
            nodeA = listA.before(nodeA);
            nodeB = listA.before(nodeB);
        }

        //handle dangling nodes
        ProjectList remList = null;
        Position remNode = null;
        boolean transversedRem = false;

        if (!transversedA) {
            remList = listA;
            remNode = nodeA;
        } else if (!transversedB) {
            remList = listB;
            remNode = nodeB;
        } else {//else we don't need to transverse rem
            transversedRem = true;
        }

        while (!transversedRem) {
            transversedRem = remList.isFirst(remNode);
            int sub = remNode.getValue() - subCarry;
            //handle if the sub goes negative
            if (sub < 0) {
                subCarry = 1;
                sub = 99999999 + sub + 1;
            }
            else {
                subCarry = 0;
            }
            //insert sub into sumList
            subList.insertFirst(sub);
            //move remNode back
            remNode = remList.before(remNode);
        }
        //handle insertion of final overflow if any
        if (subCarry > 0) {
            subList.insertFirst(99999999 - subCarry);
        }
        //result returned is same sign as larger
        LongInteger result = new LongInteger(subList, isNeg);
        return result;

    }

    /**
     * Create and return a clone of this LongInteger
     * @return a clone of this LongInteger
     */
    public LongInteger clone() { return new LongInteger(list, isNeg); }


    /**
     * Returns the product of this LongInteger and the given LongInteger.
     * @param i LongInteger to multiply this LongInteger by.
     * @return LongInteger that represents the product of this and i.
     */
    public LongInteger multiply(LongInteger i) {
        LongInteger zero = new LongInteger("0");
        if (this.equalTo(zero) || i.equalTo(zero))
            return zero;

        ProjectList larger, smaller;
        if (this.lessThan(i)) {
            larger = i.list;
            smaller = this.list;
        } else {
            larger = this.list;
            smaller = i.list;
        }

        //handle negatives
        boolean isNeg = true;//negative by default
        if ((this.isNeg && i.isNeg) || (!this.isNeg && !i.isNeg))
            isNeg = false;
        //create a new ProjectList for the product
        LongInteger product = new LongInteger("0");
        int multSet = 0;//to count which node we are on
        Position smlNode = smaller.last();
        //transverse from least significant digit to most
        while (smlNode != null) {
            int v1 = 0;//multiplication carry
            ProjectList tempMult = new DLLProjectList();//temp list to hold the current node's product
            Position lrgNode = larger.last();
            while (lrgNode != null) {
                //implement one-step Karatsuba
                int z1 = UtilityOperations.upperHalf(lrgNode.getValue()) *
                        UtilityOperations.upperHalf(smlNode.getValue());
                int z3 = UtilityOperations.lowerHalf(lrgNode.getValue()) *
                        UtilityOperations.lowerHalf(smlNode.getValue());
                int z2 = ((UtilityOperations.upperHalf(lrgNode.getValue()) + UtilityOperations.lowerHalf(lrgNode.getValue())) *
                        (UtilityOperations.upperHalf(smlNode.getValue()) + UtilityOperations.lowerHalf(smlNode.getValue())))
                        - z1 - z3;
                //create two new nodes
                int v2 = z3 + (UtilityOperations.lowerHalf(z2) * 10000) + v1;//add v1 as from carry
                v1 = z1 + UtilityOperations.upperHalf(z2) +
                    UtilityOperations.overflow(v2);//add the overflow from v2
                //set v2 to it's underflow
                v2 = UtilityOperations.underflow(v2);

                tempMult.insertFirst(v2);
                //move the node towards the most significant end
                lrgNode = larger.before(lrgNode);
            }
            //add significance to the tempMult
            for (int j = 0; j < multSet; j++) {
                tempMult.insertLast(0);
            }
            multSet++;//increment the significance of the next tempMult
            //handle hanging carries on last step
            if (v1 > 0) {
                tempMult.insertFirst(v1);
            }
            //add tempMult to the product
            product = product.add(new LongInteger(tempMult, false));
            //move towards the most significant node
            smlNode = smaller.before(smlNode);
        }
        //create new product with corrected sign and return
        product = new LongInteger(product.list, isNeg);
        return product;
    }

    /*
    public LongInteger power(int p) {

    }
    */
}
