public class UtilityOperations {

    public static int overflow(int t) {  return (t / 100000000); }
    
    public static int underflow(int t) { return (t % 100000000); }


    
    public static int upperHalf(int t) {
        return (t / 10000);
    }
    
    public static int lowerHalf(int t) {
        return (t % 10000);
    }
    
    public static int digits(int t) {
        if (t == 0) return 1;
        int digits = 1;
        while ((t/=10) != 0) { ++digits; }
        return digits;
    }


    public static int absoluteValue(int a) {
        //negative case
        if (a < 0) {
            return (-1) * a;
        }
        //positive case
        return a;
    }
}
