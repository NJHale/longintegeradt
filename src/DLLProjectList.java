public class DLLProjectList implements ProjectList<DLLNode> {

    private DLLNode head;
    private DLLNode tail;

    private int size;

    public DLLProjectList() {
        head = null;
        tail = head;
        size = 0;
    }
    

    public void insertFirst(int value) {
        DLLNode node = new DLLNode(value, null, null);
        //handle special case if the list is empty
        if (head == null) {
            head = node;
            tail = node;
        } else {//handle general case
            node.setNext(head);
            head.setPrev(node);
            head = node;
        }
        size++;
    }
    
    public void insertLast(int value) {
        DLLNode node = new DLLNode(value, null, null);
        //handle special case if the list is empty
        if (tail == null) {
            tail = node;
            head = node;
        } else {//handle general case
            node.setPrev(tail);
            tail.setNext(node);
            tail = node;
        }
        size++;
    }

    public DLLNode first() {
        return head;
    }

    public DLLNode last() {
        return tail;
    }

    public boolean isFirst(DLLNode p) {
        return (p == head);
    }

    public boolean isLast(DLLNode p) { return (p == tail); }

    public DLLNode before(DLLNode p) {
        return p.getPrev();
    }

    public DLLNode after(DLLNode p) {
        return p.getNext();
    }

    public boolean isEmpty() {
        return (head == null);
    }

    public int size() {
        return size;
    }

}
