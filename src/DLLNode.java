public class DLLNode extends Position {
    private DLLNode next;
    private DLLNode prev;

    /***
     * Constructs a DLLNode
     * @param value - Value of the DLLNode
     * @param next - The DLLNode that succeeds this DLLNode
     * @param prev - The DLLNode that precedes this DLLNode
     */
    public DLLNode(int value, DLLNode next, DLLNode prev) {
        super(value);

		this.next = next;
		this.prev = prev;
    }

    /**
     * Gets the DLLNode's succeeding DLLNode
     * @return - The DLLNode that succeeds this DLLNode
     */
    public DLLNode getNext() {
        return next;
    }

    /**
     * Gets the DLLNode's preceding DLLNode
     * @return - The DLLNode that precedes this DLLNode
     */
    public DLLNode getPrev() {
        return prev;
    }

    /**
     * Sets the succeeding DLLNode
     * @param next - DLLNode to succeed this DLLNode
     */
    public void setNext(DLLNode next) {
        this.next = next;
    }

    /**
     * Sets the preceding DLLNode
     * @param prev - DLLNode to precede this DLLNode
     */
    public void setPrev(DLLNode prev) {
        this.prev = prev;
    }
}
