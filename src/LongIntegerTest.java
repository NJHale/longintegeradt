import java.util.Dictionary;
import java.util.Hashtable;

import static org.junit.Assert.*;

/**
 * Created by Nick on 3/31/2015.
 */
public class LongIntegerTest {

    private Dictionary longIntDict;

    @org.junit.Before
    public void setUp() throws Exception {
        //instantiate test cases
        longIntDict = new Hashtable();
        longIntDict.put('A', new LongInteger("2222"));
        longIntDict.put('B', new LongInteger("99999999"));
        longIntDict.put('C', new LongInteger("-246813575732"));
        longIntDict.put('D', new LongInteger("180270361023456789"));
        longIntDict.put('E', new LongInteger("1423550000000010056810000054593452907711568359"));
        longIntDict.put('F', new LongInteger("-350003274594847454317890"));
        longIntDict.put('G', new LongInteger("29302390234702973402973420937420973420937420937234872349872934872893472893749287423847"));
        longIntDict.put('H', new LongInteger("-98534342983742987342987339234098230498203894209928374662342342342356723423423"));
    }

    @org.junit.After
    public void tearDown() throws Exception {

    }

    @org.junit.Test
    public void testAdd()throws Exception {
        //test basic add
        LongInteger a = (LongInteger)longIntDict.get('A');
        LongInteger b = (LongInteger)longIntDict.get('B');
        //get a + b
        LongInteger sum = a.add(b);
        LongInteger expected = new LongInteger("100002221");
        //check against expected
        assertTrue(sum.equalTo(expected));
    }

    @org.junit.Test
    public void testBothNegativeAdd()throws Exception {
        //test negative + negative
        LongInteger a = (LongInteger)longIntDict.get('C');
        LongInteger b = (LongInteger)longIntDict.get('F');
        //get a + b
        LongInteger sum = a.add(b);
        LongInteger expected = new LongInteger("-350003274595094267893622");
        //check against expected
        assertTrue(sum.equalTo(expected));
    }

    @org.junit.Test
    public void testSubtract() throws Exception {
        //test basic subtract
        LongInteger a = (LongInteger)longIntDict.get('A');
        LongInteger b = (LongInteger)longIntDict.get('B');
        //get b - a
        LongInteger sub = b.subtract(a);
        LongInteger expected = new LongInteger("99997777");
        //check against expected
        assertTrue(sub.equalTo(expected));
    }

    @org.junit.Test
    public void testPosToNegSubtract() throws Exception {
        //test basic subtract
        LongInteger a = (LongInteger)longIntDict.get('B');
        LongInteger b = (LongInteger)longIntDict.get('C');
        //get b - a
        LongInteger sub = b.subtract(a);
        LongInteger expected = new LongInteger("-246913575731");
        //check against expected
        //sub.output();
        assertTrue(sub.equalTo(expected));
    }

    @org.junit.Test
    public void testMultiNodalSubtract() throws Exception {
        //test basic subtract
        LongInteger a = (LongInteger)longIntDict.get('G');
        LongInteger b = (LongInteger)longIntDict.get('E');
        //get b - a
        LongInteger sub = a.subtract(b);
        LongInteger expected = new LongInteger("29302390234702973402973420937420973420935997387234872339816124872838879440841575855488");
        //check against expected
        assertTrue(sub.equalTo(expected));
    }

    @org.junit.Test
     public void testNegativeMultiNodalSubtract() throws Exception {
        //test basic subtract
        LongInteger a = (LongInteger)longIntDict.get('G');
        LongInteger b = (LongInteger)longIntDict.get('E');
        //get b - a
        LongInteger sub = b.subtract(a);
        LongInteger expected = new LongInteger("-29302390234702973402973420937420973420935997387234872339816124872838879440841575855488");
        //check against expected
        assertTrue(sub.equalTo(expected));
    }

    @org.junit.Test
    public void testNegNegMultiNodalSubtract() throws Exception {
        //test basic subtract
        LongInteger a = (LongInteger)longIntDict.get('H');
        LongInteger b = (LongInteger)longIntDict.get('F');
        //get b - a
        LongInteger sub = a.subtract(b);
        LongInteger expected = new LongInteger("-98534342983742987342987339234098230498203894209928374312339067747509269105533");
        //check against expected
        assertTrue(sub.equalTo(expected));
    }

    @org.junit.Test
    public void testBasicMultiply() throws Exception {
        //test basic single node multiply
        LongInteger a = new LongInteger("10");
        LongInteger b = new LongInteger("10");
        //get a * b
        LongInteger product = a.multiply(b);
        product.output();
        LongInteger expected = new LongInteger("100");
        //check against expected
        assertTrue(product.equalTo(expected));
    }

    @org.junit.Test
    public void testBigMultiply() throws Exception {
        //test basic single node multiply
        LongInteger a = new LongInteger("10000000000000000000000000000000");
        LongInteger b = new LongInteger("100000000000000000000000000000000000000");
        //get a * b
        LongInteger product = a.multiply(b);
        product.output();
        LongInteger expected = new LongInteger("1000000000000000000000000000000000000000000000000000000000000000000000");
        //check against expected
        assertTrue(product.equalTo(expected));
    }

    @org.junit.Test
    public void testGetDigitCount() throws Exception {

    }

    @org.junit.Test
    public void testEqualTo() throws Exception {

    }

    @org.junit.Test
    public void testLessThan() throws Exception {

    }

    @org.junit.Test
    public void testGreaterThan() throws Exception {

    }
}